//
//  AppDelegate.h
//  CatchMe
//
//  Created by Marcel Zanoni on 12.06.13.
//  Copyright (c) 2013 Mobile Computing / Cermenika / Zanoni / Zotter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
