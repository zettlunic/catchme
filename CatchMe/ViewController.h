//
//  ViewController.h
//  CatchMe
//
//  Created by Marcel Zanoni on 12.06.13.
//  Copyright (c) 2013 Mobile Computing / Cermenika / Zanoni / Zotter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

#define kNumberOfMarkers 1

@interface ViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate> {
    CLLocationManager *locationManager;
    CLLocation *userLoc;
    CLLocation *initLoc;
    
    NSMutableArray *randomLocs;
    BOOL ready;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet UIView *shakeView;

@end
