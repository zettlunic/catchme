//
//  main.m
//  CatchMe
//
//  Created by Marcel Zanoni on 12.06.13.
//  Copyright (c) 2013 Mobile Computing / Cermenika / Zanoni / Zotter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
