//
//  ViewController.m
//  CatchMe
//
//  Created by Marcel Zanoni on 12.06.13.
//  Copyright (c) 2013 Mobile Computing / Cermenika / Zanoni / Zotter. All rights reserved.
//

#import "ViewController.h"
#import "CMScoreManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(48.239, 16.377);
    //    MKCoordinateSpan span = MKCoordinateSpanMake(.015, .015);
    //    MKCoordinateRegion region = MKCoordinateRegionMake(loc, span);
    //    self.mapView.region = region;
    
    randomLocs = [[NSMutableArray alloc] init];
    
    self.mapView.delegate = self;
    
    self.scoreLabel.text = [NSString stringWithFormat:@"%d", [[CMScoreManager sharedManager] getScore]];
    
    [self.shakeView setUserInteractionEnabled:NO];
}

- (void) viewDidAppear:(BOOL)animated {
    NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    [self initLocationManagerWithAuth];
    
    self.shakeView.layer.cornerRadius = 5;
    self.shakeView.layer.masksToBounds = YES;
    
    self.scoreView.layer.cornerRadius = 5;
    self.scoreView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Shake control

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    [self.shakeView setHidden:YES];
    NSLog(@"Hey you shook me!");
    
    if (!ready) {
        return;
    }
    
    if ([randomLocs count] >= 1) { //user didn't collect all points
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                            message:[NSString stringWithFormat:@"You have %d marks left to collect, if you continue you will loose points", [randomLocs count]]
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Continue", nil];
        
        [alertView show];
    }
    //the user has collected all 3 points
    else
    {
//        [self addPinsToMap:self.mapView amount:3];
        [self addPinsToMap:self.mapView amount:kNumberOfMarkers];
    }

    NSLog(@"%@", [NSNumber numberWithInt:[randomLocs count]]);
}

//called by UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%d", buttonIndex);
    int score = [[CMScoreManager sharedManager] getScore];
    
    switch (buttonIndex) {
        case 0:
            break;
            
        case 1: //continue pressed
            //remove all objects from array and map
            for (MKCircle* loc in randomLocs) {
                [self.mapView removeOverlay:loc];
            }
            [randomLocs removeAllObjects];
            
            //update score
            if (score > 0) {
                [self updateScoreWithInt:score - 1];
            }
            
            //add new pins
//            [self addPinsToMap:self.mapView amount:3];
            [self addPinsToMap:self.mapView amount:kNumberOfMarkers];
            break;
            
        default:
            break;
    }
}

#pragma mark - Location manager

- (void)initLocationManagerWithAuth
{
    BOOL ok = [CLLocationManager locationServicesEnabled];
    if (!ok) {
        //catch error
        [self locationAuthError];
        ready = NO;
        return;
    }
    CLAuthorizationStatus auth = [CLLocationManager authorizationStatus];
    if (auth == kCLAuthorizationStatusRestricted || auth == kCLAuthorizationStatusDenied) {
        //catch error
        [self locationAuthError];
        ready = NO;
        return;
    }
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    ready = YES;
    [self.shakeView setHidden:NO];
}

- (void)locationAuthError
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Location services disabled"
                                                        message:@"Active the location service for CatchMe to start playing"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = [locations lastObject];
    
    if (userLoc != nil) {
        userLoc = loc;
        [self checkIfPointReached];
        
        if ([userLoc distanceFromLocation:initLoc] > 100) {
            NSLog(@"getting away!!");
            
            MKCoordinateSpan span = MKCoordinateSpanMake(.003, .003);
            MKCoordinateRegion region = MKCoordinateRegionMake(userLoc.coordinate, span);
            [self.mapView setRegion:region animated:YES];
            initLoc = loc;
        }
    } else {
        userLoc = loc;
        initLoc = loc;
        NSLog(@"first location");
        
        MKCoordinateSpan span = MKCoordinateSpanMake(.003, .003);
        MKCoordinateRegion region = MKCoordinateRegionMake(userLoc.coordinate, span);
        [self.mapView setRegion:region animated:YES];
        
        //[self addPinsToMap:self.mapView amount:10];
        
    }
}

#pragma mark - Game logic and handlers

- (void) checkIfPointReached
{
    NSMutableArray *removals = [NSMutableArray new];
    
    for (MKCircle *loc in randomLocs) {
        CLLocation *randLoc = [[CLLocation alloc] initWithLatitude:loc.coordinate.latitude longitude:loc.coordinate.longitude];
        if ([userLoc distanceFromLocation:randLoc] < 17) {
            NSLog(@"GOT IT!!!!!!");
            
            [self.mapView removeOverlay:loc];
            [removals addObject:loc];
            
            [self updateScoreWithInt:[[CMScoreManager sharedManager] getScore] + 1];
        }
    }
    
    [self removeMarkers:removals]; //clean up the cathed points
}

- (void) removeMarkers: (NSMutableArray*) markers
{
    if ([markers count] == 0) {
        return; //nothing to remove
    }
    
    for (MKCircle *loc in markers) {
        [randomLocs removeObject:loc];
    }
    
    if ([randomLocs count] == 0) {  //user collected all markers
        [self.shakeView setHidden:NO];
    }
}

- (void) addPinsToMap:(MKMapView *)mapView amount:(int)howMany
{
    // Loop
    for (int i = 0; i < howMany; i++) {
//        double latRange = [self randomFloatBetween:neCoord.latitude andBig:swCoord.latitude];
//        double longRange = [self randomFloatBetween:neCoord.longitude andBig:swCoord.longitude];
        
        CLLocationCoordinate2D userCord = mapView.userLocation.location.coordinate;
        
        double latRange = [self randomFloatBetween:userCord.latitude - .001 andBig:userCord.latitude + .001];
        double longRange = [self randomFloatBetween:userCord.longitude - .001 andBig:userCord.longitude + .001];
        
        NSLog(@"Lat %f", latRange);
        NSLog(@"Long %f", longRange);
        
        // Add new waypoint to map
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(latRange, longRange);
        //        MKPointAnnotation *ann = [MKPointAnnotation new];
        //        ann.coordinate = location;
        //        ann.title = @"Hey!";
        //        ann.subtitle = @"Get your ass here";
        //        [self.mapView addAnnotation:ann];
        
        MKCircle *circle = [MKCircle circleWithCenterCoordinate:location radius:15];
        [self.mapView addOverlay:circle];
        
        [randomLocs addObject:circle];
        
    }//end
    
}

- (void) updateScoreWithInt:(int) value
{
    [[CMScoreManager sharedManager] setScoreWithInt:value];
    self.scoreLabel.text = [NSString stringWithFormat:@"%d", [[CMScoreManager sharedManager] getScore]];
}

#pragma mark - Map view methods

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
	if([overlay isKindOfClass:[MKCircle class]]) {
		// Create the view for the radius overlay.
		MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
		circleView.strokeColor = [UIColor purpleColor];
		circleView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.4];
		
		return circleView;
	}
	
	return nil;
}

#pragma mark Help Methods

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

/**
 * Random numbers
 *
 * @version $Revision: 0.1
 */
- (double)randomFloatBetween:(double)smallNumber andBig:(double)bigNumber
{
    double diff = bigNumber - smallNumber;
    return (((double) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

@end