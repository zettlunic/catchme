//
//  CMScoreManager.h
//  CatchMe
//
//  Created by Marcel Zanoni on 23.06.13.
//  Copyright (c) 2013 Mobile Computing / Cermenika / Zanoni / Zotter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CMScoreManager : NSObject

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (int) getScore;
- (BOOL) setScoreWithInt: (int) value;

+ (CMScoreManager *)sharedManager;

@end
